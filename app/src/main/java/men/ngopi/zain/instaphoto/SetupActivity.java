package men.ngopi.zain.instaphoto;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Build;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.design.widget.TextInputEditText;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.google.android.gms.tasks.Continuation;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.UserProfileChangeRequest;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import com.theartofdev.edmodo.cropper.CropImage;
import com.theartofdev.edmodo.cropper.CropImageView;

import java.io.ByteArrayOutputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;

import id.zelory.compressor.Compressor;

public class SetupActivity extends AppCompatActivity implements View.OnClickListener {

    private ImageView imageView;
    private TextInputEditText txtName;
    private Button btnSetup;
    private ProgressBar progressBar;

    private Uri mainImageUri;
    private Boolean isChanged = false;

    private FirebaseUser firebaseUser;
    private FirebaseFirestore firebaseFirestore;
    private StorageReference storageReference;
    private Uri profileImageUri;

    private FirebaseAuth mAuth;
    private Intent intent;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_setup);

        intent = getIntent();

        mAuth = FirebaseAuth.getInstance();

        imageView = findViewById(R.id.profile_image);
        txtName = findViewById(R.id.setupName);
        btnSetup = findViewById(R.id.setupBtn);
        progressBar = findViewById(R.id.setupProgress);
        progressBar.setVisibility(View.INVISIBLE);

        btnSetup.setOnClickListener(this);
        imageView.setOnClickListener(this);
    }


    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.profile_image:
                if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.M){
                    if(ContextCompat.checkSelfPermission(SetupActivity.this, Manifest.permission.READ_EXTERNAL_STORAGE)
                            != PackageManager.PERMISSION_GRANTED){
                        ActivityCompat.requestPermissions(SetupActivity.this, new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, 1);
                        ActivityCompat.requestPermissions(SetupActivity.this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, 1);
                    } else {
                        BringImagePicker();
                    }
                } else {
                    BringImagePicker();
                }
                break;
            case R.id.setupBtn:
                if(!txtName.getText().toString().isEmpty() && isChanged){
                    register(intent.getStringExtra("email"), intent.getStringExtra("pass"));
                } else {
                    Toast.makeText(SetupActivity.this, "Enter a photo and name", Toast.LENGTH_SHORT).show();
                }
                break;
        }
    }

    private void uploadProfile(){
        firebaseUser = mAuth.getCurrentUser();
        storageReference = FirebaseStorage.getInstance().getReference();
        firebaseFirestore = FirebaseFirestore.getInstance();
        final UserProfileChangeRequest profileUpdates = new UserProfileChangeRequest.Builder()
                .setDisplayName(txtName.getText().toString())
                .setPhotoUri(mainImageUri)
                .build();

        firebaseUser.updateProfile(profileUpdates).addOnCompleteListener(new OnCompleteListener<Void>() {
            @Override
            public void onComplete(@NonNull Task<Void> task) {
                if(task.isSuccessful()){
                    final StorageReference image_path = storageReference.child("profile_image").child(firebaseUser.getUid() + ".jpeg");

                    image_path.putFile(firebaseUser.getPhotoUrl()).continueWithTask(new Continuation<UploadTask.TaskSnapshot, Task<Uri>>() {
                        @Override
                        public Task<Uri> then(@NonNull Task<UploadTask.TaskSnapshot> task) throws Exception {
                            if(!task.isSuccessful()){
                                throw task.getException();
                            }
                            return image_path.getDownloadUrl();
                        }
                    }).addOnCompleteListener(new OnCompleteListener<Uri>() {
                        @Override
                        public void onComplete(@NonNull Task<Uri> task) {
                            if(task.isSuccessful()){
                                profileImageUri = task.getResult();

                                Map<String, String> userMap = new HashMap<>();
                                userMap.put("id", firebaseUser.getUid());
                                userMap.put("name", firebaseUser.getDisplayName());
                                userMap.put("photo", profileImageUri.toString());

                                firebaseFirestore.collection("Users").document(firebaseUser.getUid()).set(userMap).addOnCompleteListener(new OnCompleteListener<Void>() {
                                    @Override
                                    public void onComplete(@NonNull Task<Void> task) {
                                        if(task.isSuccessful()){
                                            Intent mainIntent = new Intent(SetupActivity.this, MainActivity.class);
                                            RegisterActivity.getInstance().finish();
                                            startActivity(mainIntent);
                                            finish();
                                        } else {
                                            Log.i("Error", "Error Upload Image Profile");
                                        }
                                    }
                                });
                            }
                        }
                    });
                } else {
                    Toast.makeText(SetupActivity.this, "Update Profile Failed", Toast.LENGTH_SHORT).show();
                    btnSetup.setVisibility(View.VISIBLE);
                    progressBar.setVisibility(View.INVISIBLE);
                }
            }
        });
    }

    private void register (String email, String password){
        progressBar.setVisibility(View.VISIBLE);
        btnSetup.setVisibility(View.INVISIBLE);
        mAuth.createUserWithEmailAndPassword(email, password)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            uploadProfile();
                        } else {
                            Toast.makeText(SetupActivity.this, "Register Failed", Toast.LENGTH_SHORT).show();

                        }
                    }
                });
    }

    private void BringImagePicker(){
        CropImage.activity()
                .setGuidelines(CropImageView.Guidelines.ON)
                .setCropShape(CropImageView.CropShape.RECTANGLE)
                .setAspectRatio(1,1)
                .setMinCropWindowSize(500,500)
                .start(SetupActivity.this);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) {
            CropImage.ActivityResult result = CropImage.getActivityResult(data);
            if (resultCode == RESULT_OK) {
                mainImageUri = result.getUri();
                imageView.setImageURI(mainImageUri);
                isChanged = true;
            } else if (resultCode == CropImage.CROP_IMAGE_ACTIVITY_RESULT_ERROR_CODE) {
                Exception error = result.getError();
            }
        }
    }
}
