package men.ngopi.zain.instaphoto;

import android.Manifest;
import android.app.Fragment;
import android.content.Intent;
import android.app.FragmentTransaction;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MenuItem;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.theartofdev.edmodo.cropper.CropImage;
import com.theartofdev.edmodo.cropper.CropImageView;

public class MainActivity extends AppCompatActivity {

    private FirebaseUser firebaseUser;
    private FirebaseAuth firebaseAuth;
    private static MainActivity mainActivity;

    private Uri mainImageUri;
    private Boolean isChanged = false;

    private Fragment selectedFragment;
    BottomNavigationView bottomNavigationView;

    public static MainActivity getInstance(){
        return mainActivity;
    }

    private static final String SHARED_PREFERENCE = "men.ngopi.zain";
    private static final String PREF_OPENED_FRAGMENT = "opened_fragment";

    private SharedPreferences sharedPreferences;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mainActivity = this;

        sharedPreferences = getSharedPreferences(SHARED_PREFERENCE, MODE_PRIVATE);


        firebaseAuth = FirebaseAuth.getInstance();
        firebaseUser = firebaseAuth.getCurrentUser();

        bottomNavigationView = findViewById(R.id.navigation);

        bottomNavigationView.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {

                int openedFragment = 0;

                switch (menuItem.getItemId()){
                    // Fragment Home
                    case R.id.action_item1:
                        selectedFragment = FragmentHome.newInstance();
                        break;
                    case R.id.action_item2:
                        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.M){
                            if(ContextCompat.checkSelfPermission(MainActivity.this, Manifest.permission.READ_EXTERNAL_STORAGE)
                                    != PackageManager.PERMISSION_GRANTED){
                                ActivityCompat.requestPermissions(MainActivity.this, new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, 1);
                                ActivityCompat.requestPermissions(MainActivity.this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, 1);
                            } else {
                                BringImagePicker();
                            }
                        } else {
                            BringImagePicker();
                        }
                        break;
                    case R.id.action_item3:
                        selectedFragment = FragmentProfile.newInstance();
                        openedFragment = 1;
                        break;
                }

                FragmentTransaction transaction = getFragmentManager().beginTransaction();
                transaction.replace(R.id.frame_layout, selectedFragment);
                transaction.commit();

                // save to shared preferences
                SharedPreferences.Editor editor = sharedPreferences.edit();
                editor.putInt(PREF_OPENED_FRAGMENT, openedFragment);
                editor.apply();

                return true;
            }
        });
    }

    @Override
    protected void onStart() {
        super.onStart();

        

        if(firebaseUser == null){
            Intent loginIntent = new Intent(this, LoginActivity.class);
            startActivity(loginIntent);
            finish();
        } else {
            int openedFragment = sharedPreferences.getInt(PREF_OPENED_FRAGMENT, 0);

            switch (openedFragment) {
                case 0: {
                    bottomNavigationView.setSelectedItemId(R.id.action_item1);
                    break;
                }
                case 1: {
                    bottomNavigationView.setSelectedItemId(R.id.action_item3);
                    break;
                }
            }
        }
    }

    private void BringImagePicker(){
        CropImage.activity()
                .setGuidelines(CropImageView.Guidelines.ON)
                .setCropShape(CropImageView.CropShape.RECTANGLE)
                .setAspectRatio(16,9)
                .start(MainActivity.this);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) {
            CropImage.ActivityResult result = CropImage.getActivityResult(data);
            if (resultCode == RESULT_OK) {
                mainImageUri = result.getUri();

                Intent uploadIntent = new Intent(MainActivity.this, UploadActivity.class);
                uploadIntent.putExtra("mainImageUri", mainImageUri.toString());
                startActivity(uploadIntent);
                isChanged = true;
            } else if (resultCode == CropImage.CROP_IMAGE_ACTIVITY_RESULT_ERROR_CODE) {
                Exception error = result.getError();
            }
        }
    }
}
