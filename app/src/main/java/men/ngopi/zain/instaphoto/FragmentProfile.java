package men.ngopi.zain.instaphoto;

import android.app.Fragment;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.firebase.ui.firestore.FirestoreRecyclerAdapter;
import com.firebase.ui.firestore.FirestoreRecyclerOptions;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.Query;
import com.squareup.picasso.Picasso;

import de.hdodenhof.circleimageview.CircleImageView;

public class FragmentProfile extends Fragment implements View.OnClickListener {

    private View view;
    private Context context;

    private FirebaseUser firebaseUser;
    private FirebaseAuth firebaseAuth;
    private FirebaseFirestore firebaseFirestore;

    private TextView txtName, txtEmail, txtIsEmpty;
    private Button btnLogout;
    private CircleImageView imgProfile;
    private ImageView ivNoPhoto;
    private String idPhoto;

    private RecyclerView listImage;
    private FirestoreRecyclerAdapter<Post, PostHolder> adapter;
    private FirestoreRecyclerOptions<Post> response;

    public static FragmentProfile newInstance(){
        FragmentProfile fragmentProfile = new FragmentProfile();
        return fragmentProfile;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_two, container, false);
        return view;
    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        firebaseAuth = FirebaseAuth.getInstance();
        firebaseUser = firebaseAuth.getCurrentUser();
        firebaseFirestore = FirebaseFirestore.getInstance();

        context = getContext();

        txtName = view.findViewById(R.id.twoName);
        txtEmail = view.findViewById(R.id.twoEmail);
        imgProfile = view.findViewById(R.id.twoImageProfile);
        btnLogout = view.findViewById(R.id.twoLogoutBtn);
        txtIsEmpty = view.findViewById(R.id.twoIsEmpty);
        ivNoPhoto = view.findViewById(R.id.twoNoPhoto);
        listImage = view.findViewById(R.id.twoRecyclerView);
        listImage.setHasFixedSize(true);
        listImage.setNestedScrollingEnabled(false);
        listImage.setLayoutManager(new GridLayoutManager(context, 3));

        txtName.setText(firebaseUser.getDisplayName());
        txtEmail.setText(firebaseUser.getEmail());
        imgProfile.setImageURI(firebaseUser.getPhotoUrl());
        ivNoPhoto.setVisibility(View.VISIBLE);
        txtIsEmpty.setText("Let's add your photo");


        btnLogout.setOnClickListener(this);
    }

    private void getPosts(){
        Query query = firebaseFirestore.collection("Posts").whereEqualTo("idUser", firebaseUser.getUid())
                .orderBy("timestamp", Query.Direction.DESCENDING);

        response = new FirestoreRecyclerOptions.Builder<Post>()
                .setQuery(query, Post.class)
                .build();

        adapter = new FirestoreRecyclerAdapter<Post, PostHolder>(response) {

            @Override
            protected void onBindViewHolder(@NonNull final PostHolder holder, int position, @NonNull Post model) {
                idPhoto = model.getId();
                if(!idPhoto.equals("")){
                    txtIsEmpty.setText("");
                    ivNoPhoto.setVisibility(View.INVISIBLE);
                }
                String imageUrl = model.getPhoto();
                Picasso.get().load(imageUrl).resize(518,518).centerCrop().into(holder.mainImg);

//                holder.itemView.setOnClickListener(new View.OnClickListener() {
//                    @Override
//                    public void onClick(View view) {
//
//                    }
//                });
            }

            @NonNull
            @Override
            public PostHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
                View view = LayoutInflater.from(viewGroup.getContext())
                        .inflate(R.layout.post_profile_row, viewGroup, false);

                return new PostHolder(view);
            }
        };

        adapter.notifyDataSetChanged();
        listImage.setAdapter(adapter);
    }

    @Override
    public void onStart() {
        super.onStart();
        getPosts();
        adapter.startListening();
    }

    @Override
    public void onStop() {
        super.onStop();
        adapter.stopListening();
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.twoLogoutBtn:
                Intent loginIntent = new Intent(view.getContext(), LoginActivity.class);
                firebaseAuth.signOut();
                startActivity(loginIntent);
                MainActivity.getInstance().finish();
                break;
        }
    }

    public class PostHolder extends RecyclerView.ViewHolder {
        private ImageView mainImg;
        public PostHolder(View itemView){
            super(itemView);
            mainImg = itemView.findViewById(R.id.rowProfileImageView);
        }
    }

}


