package men.ngopi.zain.instaphoto;

import android.app.Fragment;
import android.content.Context;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.firebase.ui.firestore.FirestoreRecyclerAdapter;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.Query;
import com.firebase.ui.firestore.FirestoreRecyclerOptions;
import com.squareup.picasso.Picasso;


public class FragmentHome extends Fragment {

    private View view;
    private Context context;
    private FirebaseFirestore firebaseFirestore;
    private RecyclerView postList;
    private FirestoreRecyclerAdapter<Post, PostHolder> adapter;
    private FirestoreRecyclerOptions<Post> response;
    private String imageUser = "", idUser;
    private String imageUrl;
    private String desc;
    private String name;
    private SwipeRefreshLayout swipeRefreshLayout;

    public static FragmentHome newInstance(){
        FragmentHome fragmentHome = new FragmentHome();
        return fragmentHome;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_one, container, false);
        return view;
    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        firebaseFirestore = FirebaseFirestore.getInstance();
        postList = view.findViewById(R.id.oneRecyclerview);
        swipeRefreshLayout = view.findViewById(R.id.swiperefresh);
        postList.setHasFixedSize(true);
        postList.setNestedScrollingEnabled(false);
        postList.setLayoutManager(new LinearLayoutManager(context));
        context = getContext();
        getPosts();
        adapter.startListening();

        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                adapter.stopListening();
                getPosts();
                adapter.startListening();
            }
        });

    }

    private void getPosts(){
        Query query = firebaseFirestore.collection("Posts")
                .orderBy("timestamp", Query.Direction.DESCENDING);

        response = new FirestoreRecyclerOptions.Builder<Post>()
                .setQuery(query, Post.class)
                .build();

        adapter = new FirestoreRecyclerAdapter<Post, PostHolder>(response) {
            @Override
            protected void onBindViewHolder(@NonNull final PostHolder holder, int position, @NonNull Post model) {
                holder.progressBar.setVisibility(View.VISIBLE);
                name = model.getName();
                desc = model.getDesc();
                imageUrl = model.getPhoto();
                idUser = model.getIdUser();
                imageUser = model.getPhotoUser();
                Log.d("Get Id User", "onEvent: "+ idUser);
                Picasso.get().load(imageUser).resize(100,100).centerCrop().into(holder.profileImg);
                Picasso.get().load(imageUrl).resize(920,518).centerCrop().into(holder.mainImg);
                holder.desc.setText(desc);
                holder.name.setText(name);

//                holder.itemView.setOnClickListener(new View.OnClickListener() {
//                    @Override
//                    public void onClick(View view) {
//
//                    }
//                });

            }

            @NonNull
            @Override
            public PostHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
                View view = LayoutInflater.from(viewGroup.getContext())
                        .inflate(R.layout.post_row, viewGroup, false);

                swipeRefreshLayout.setRefreshing(false);
                return new PostHolder(view);
            }
        };

        adapter.notifyDataSetChanged();
        postList.setAdapter(adapter);

    }

    public class PostHolder extends RecyclerView.ViewHolder {
        private TextView name, desc;
        private ImageView profileImg, mainImg;
        private ProgressBar progressBar;
        public PostHolder(View itemView){
            super(itemView);
            progressBar = itemView.findViewById(R.id.oneProgress);
            name = itemView.findViewById(R.id.oneName);
            desc = itemView.findViewById(R.id.oneDesc);
            profileImg = itemView.findViewById(R.id.oneImageProfile);
            mainImg = itemView.findViewById(R.id.oneImage);
        }
    }

    @Override
    public void onStop() {
        super.onStop();
        adapter.stopListening();
    }
}
