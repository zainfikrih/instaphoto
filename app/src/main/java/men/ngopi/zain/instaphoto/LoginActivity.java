package men.ngopi.zain.instaphoto;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.design.widget.TextInputEditText;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;

public class LoginActivity extends AppCompatActivity implements View.OnClickListener {

    private TextInputEditText txtEmail, txtPass;
    private Button btnLogin, btnRegister;
    private ProgressBar progressBar;

    private FirebaseAuth mAuth;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        mAuth = FirebaseAuth.getInstance();

        txtEmail = findViewById(R.id.loginEmail);
        txtPass = findViewById(R.id.loginPass);
        btnLogin = findViewById(R.id.loginBtn);
        btnRegister = findViewById(R.id.loginRegBtn);
        progressBar = findViewById(R.id.loginProgress);
        progressBar.setVisibility(View.INVISIBLE);

        btnRegister.setOnClickListener(this);
        btnLogin.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.loginRegBtn:
                Intent regIntent = new Intent(this, RegisterActivity.class);
                startActivity(regIntent);
                break;
            case R.id.loginBtn:
                if(!txtEmail.getText().toString().isEmpty()){
                    if(!txtPass.getText().toString().isEmpty()){
                        btnLogin.setVisibility(View.INVISIBLE);
                        progressBar.setVisibility(View.VISIBLE);
                        login(txtEmail.getText().toString(), txtPass.getText().toString());
                    } else {
                        txtPass.setError("Fill in this field");
                    }
                } else {
                    txtEmail.setError("Fill in this field");
                    txtPass.setError("Fill in this field");
                }
        }
    }

    private void login (String email, String password){
        mAuth.signInWithEmailAndPassword(email, password)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            Toast.makeText(LoginActivity.this, "Login Success", Toast.LENGTH_SHORT).show();
                            Intent mainIntent = new Intent(LoginActivity.this, MainActivity.class);
                            startActivity(mainIntent);
                            finish();
                        } else {
                            Toast.makeText(LoginActivity.this, "Login Failed", Toast.LENGTH_SHORT).show();
                        }
                        btnLogin.setVisibility(View.VISIBLE);
                        progressBar.setVisibility(View.INVISIBLE);
                    }
                });
    }
}
