package men.ngopi.zain.instaphoto;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.design.widget.TextInputEditText;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

public class RegisterActivity extends AppCompatActivity implements View.OnClickListener {

    private TextInputEditText txtEmail, txtPass, txtConfirmPass;
    private Button btnRegister, btnLogin;
    private ProgressBar progressBar;

    private FirebaseAuth mAuth;
    static private RegisterActivity registerActivity;

    public static RegisterActivity getInstance (){
        return registerActivity;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);

        registerActivity = this;

        mAuth = FirebaseAuth.getInstance();

        txtEmail = findViewById(R.id.regEmail);
        txtPass = findViewById(R.id.regPass);
        txtConfirmPass = findViewById(R.id.regPassConfirm);
        btnRegister = findViewById(R.id.regBtn);
        btnLogin = findViewById(R.id.regLoginBtn);
        progressBar = findViewById(R.id.regProgress);
        progressBar.setVisibility(View.INVISIBLE);

        btnLogin.setOnClickListener(this);
        btnRegister.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.regLoginBtn:
                Intent loginIntent = new Intent(this, LoginActivity.class);
                loginIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(loginIntent);
                finish();
                break;
            case R.id.regBtn:
                if(!txtEmail.getText().toString().isEmpty()){
                    if (!txtPass.getText().toString().isEmpty()){
                        if(txtConfirmPass.getText().toString().equals(txtPass.getText().toString())){
                            btnRegister.setVisibility(View.INVISIBLE);
                            progressBar.setVisibility(View.VISIBLE);
//                            register(txtEmail.getText().toString(), txtPass.getText().toString());
//                            Toast.makeText(RegisterActivity.this, "Register Success", Toast.LENGTH_SHORT).show();
                            Intent setupIntent = new Intent(RegisterActivity.this, SetupActivity.class);
                            setupIntent.putExtra("email", txtEmail.getText().toString());
                            setupIntent.putExtra("pass", txtPass.getText().toString());
                            startActivity(setupIntent);
                        } else {
                            txtConfirmPass.setError("Password does not match");
                        }
                    } else {
                        txtPass.setError("Fill in this field");
                        txtConfirmPass.setError("Fill in this field");
                    }
                } else {
                    txtEmail.setError("Fill in this field");
                    txtPass.setError("Fill in this field");
                    txtConfirmPass.setError("Fill in this field");
                }
        }
    }


}
