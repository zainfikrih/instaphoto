package men.ngopi.zain.instaphoto;

import android.annotation.SuppressLint;
import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.support.annotation.NonNull;
import android.support.annotation.RequiresApi;
import android.support.design.widget.TextInputEditText;
import android.support.v4.app.NotificationCompat;
import android.app.NotificationManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;

import com.google.android.gms.tasks.Continuation;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.Timestamp;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QuerySnapshot;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.OnProgressListener;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;

import java.sql.Time;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class UploadActivity extends AppCompatActivity implements View.OnClickListener {

    private Toolbar toolbar;
    private ImageView imageView;
    private TextInputEditText textInputEditText;
    private Button button;
    private Uri mainImageUri;
    private ProgressBar progressBar;
    private String photoImageUrl;

    private FirebaseUser firebaseUser;
    private StorageReference storageReference;
    private FirebaseFirestore firebaseFirestore;

    private NotificationCompat.Builder builder;
    private NotificationManager mNotificationManager;
    private Notification notification;
    private NotificationChannel mChannel;
    private static final int NOTIF_ID = 0;
    private static final String CHANNEL_ID = "men_ngopi_instaphoto";

    @RequiresApi(api = Build.VERSION_CODES.O)
    @SuppressLint("ServiceCast")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_upload);

        firebaseUser = FirebaseAuth.getInstance().getCurrentUser();
        storageReference = FirebaseStorage.getInstance().getReference();
        firebaseFirestore = FirebaseFirestore.getInstance();

        mainImageUri = Uri.parse(getIntent().getStringExtra("mainImageUri"));

        progressBar = findViewById(R.id.uploadProgress);
        textInputEditText = findViewById(R.id.uploadTxt);
        button = findViewById(R.id.uploadBtn);
        imageView = findViewById(R.id.uploadImage);
        imageView.setImageURI(mainImageUri);
        toolbar = findViewById(R.id.uploadToolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("Add Photo");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        //Pending Intent
        Intent intent = new Intent(this, MainActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, intent, 0);

        mChannel = null;
        mNotificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

        //Oreo
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            mChannel = new NotificationChannel(CHANNEL_ID, "Instaphoto", NotificationManager.IMPORTANCE_DEFAULT);
        }

        builder = new NotificationCompat.Builder(this, CHANNEL_ID)
                .setSmallIcon(R.drawable.ic_lens2)
                .setContentTitle("Add Photo")
                .setContentText("Uploading photos")
                .setContentIntent(pendingIntent)
                .setAutoCancel(true);
        builder.setProgress(100, 0, true);
        mNotificationManager.createNotificationChannel(mChannel);

        progressBar.setVisibility(View.INVISIBLE);
        button.setOnClickListener(this);
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.uploadBtn:
                mNotificationManager.notify(NOTIF_ID, builder.build());
                progressBar.setVisibility(View.VISIBLE);
                button.setVisibility(View.INVISIBLE);
                getPhotoUserUrl();
                break;
        }
    }

    private void getPhotoUserUrl(){
        firebaseFirestore.collection("Users").whereEqualTo("id", firebaseUser.getUid()).get().addOnSuccessListener(new OnSuccessListener<QuerySnapshot>() {
            @Override
            public void onSuccess(QuerySnapshot queryDocumentSnapshots) {
                for(DocumentSnapshot doc : queryDocumentSnapshots){
                    photoImageUrl = doc.getString("photo");
                }
                uploadImage();
            }
        });
    }

    private void uploadImage(){
        final Long timestamp = Timestamp.now().getSeconds();
        final StorageReference image_path = storageReference.child("post").child(firebaseUser.getUid() + timestamp.toString() + ".jpg");

        image_path.putFile(mainImageUri).addOnProgressListener(new OnProgressListener<UploadTask.TaskSnapshot>() {
            @Override
            public void onProgress(UploadTask.TaskSnapshot taskSnapshot) {
                double prgress = (100.0 * taskSnapshot.getBytesTransferred()) / taskSnapshot.getTotalByteCount();
                builder.setContentText(String.valueOf((int) prgress)+ " %")
                        .setProgress(100, (int)prgress, false);
                mNotificationManager.notify(NOTIF_ID, builder.build());
            }
        }).continueWithTask(new Continuation<UploadTask.TaskSnapshot, Task<Uri>>() {
            @Override
            public Task<Uri> then(@NonNull Task<UploadTask.TaskSnapshot> task) throws Exception {
                if(!task.isSuccessful()){
                    throw task.getException();
                }
                return image_path.getDownloadUrl();
            }
        }).addOnCompleteListener(new OnCompleteListener<Uri>() {
            @Override
            public void onComplete(@NonNull Task<Uri> task) {
                if(task.isSuccessful()){
                    Map<String, String> userMap = new HashMap<>();
                    userMap.put("id", firebaseUser.getUid() + timestamp.toString());
                    userMap.put("idUser", firebaseUser.getUid());
                    userMap.put("name", firebaseUser.getDisplayName());
                    userMap.put("photo", task.getResult().toString());
                    userMap.put("desc", textInputEditText.getText().toString());
                    userMap.put("photoUser", photoImageUrl);
                    userMap.put("timestamp", timestamp.toString());

                    firebaseFirestore.collection("Posts").document().set(userMap).addOnCompleteListener(new OnCompleteListener<Void>() {
                        @Override
                        public void onComplete(@NonNull Task<Void> task) {
                            if(task.isSuccessful()){
                                builder.setContentText("Upload complete")
                                        .setProgress(0, 0, false);
                                mNotificationManager.notify(NOTIF_ID, builder.build());
//                                Intent mainIntent = new Intent(UploadActivity.this, MainActivity.class);
//                                startActivity(mainIntent);
                                finish();
                            } else {
                                Log.i("Error", "Error Upload Image Profile");
                                progressBar.setVisibility(View.INVISIBLE);
                                button.setVisibility(View.VISIBLE);
                            }
                        }
                    });
                } else {
                    Log.i("Error", "Error Upload");
                    progressBar.setVisibility(View.INVISIBLE);
                    button.setVisibility(View.VISIBLE);
                }
            }
        });
    }
}
