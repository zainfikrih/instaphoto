package men.ngopi.zain.instaphoto;

public class Post {
    private String desc, id, idUser, name, photo, timestamp, photoUser;

    public Post(){

    }

    public Post(String desc, String id, String idUser, String name, String photo, String timestamp, String photoUser) {
        this.desc = desc;
        this.id = id;
        this.idUser = idUser;
        this.name = name;
        this.photo = photo;
        this.timestamp = timestamp;
        this.photoUser = photoUser;
    }

    public void setPhotoUser(String photoUser) {
        this.photoUser = photoUser;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public void setId(String id) {
        this.id = id;
    }

    public void setIdUser(String idUser) {
        this.idUser = idUser;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }

    public void setTimestamp(String timestamp) {
        this.timestamp = timestamp;
    }

    public String getPhotoUser() {
        return photoUser;
    }

    public String getDesc() {
        return desc;
    }

    public String getId() {
        return id;
    }

    public String getIdUser() {
        return idUser;
    }

    public String getName() {
        return name;
    }

    public String getPhoto() {
        return photo;
    }

    public String getTimestamp() {
        return timestamp;
    }
}
